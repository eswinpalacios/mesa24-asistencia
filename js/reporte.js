$(function() {
    $( "#date_start" ).datepicker();
    $( "#date_start" ).datepicker( "option", "dateFormat", "yy-mm-dd" ).datepicker( "setDate", new Date());

    $( "#date_finish" ).datepicker();
    $( "#date_finish" ).datepicker( "option", "dateFormat", "yy-mm-dd" ).datepicker( "setDate", new Date());

    generar_reporte();
    
});

$( "#btn-generar-reporte" ).click(function() {
    generar_reporte();
});

function generar_reporte()
{
    console.log("generar reporte");

    $.post( "reporte-listado.php", { date_start: $("#date_start").val(), date_finish: $("#date_finish").val() })
    .done(function( data ) {
        $( "#reporte" ).html( data );
    });
}