$( "#dni" ).keypress(function(event) {
    if ( event.which == 13 ) {        
            marcar_asistencia();
    }
});

$( "#btn-marcar" ).click(function() {	
    marcar_asistencia();
});

function marcar_asistencia()
{
    if($("#dni").val().length!=8)
        return;

    console.log("marcar asistencia");

    $.post( "marcar-asistencia.php", { dni: $("#dni").val() })
    .done(function( data ) {
        console.log(data);

        var response = JSON.parse(data);
        var alerts = "";

        if(response.state==0)
        {
            alerts = "<div class='alert alert-dismissible alert-success'>" + 
            "<button type='button' class='close' data-dismiss='alert'>x</button>"+
            "<span class='label label-info'>"+ response.date +"</span> <strong>Correcto </strong> " + response.msj +"</div>";
        }
        else if(response.state==1)
        {
            alerts = "<div class='alert alert-dismissible alert-warning'>" + 
            "<button type='button' class='close' data-dismiss='alert'>x</button>" + 
            "<span class='label label-info'>"+ response.date +"</span> <strong>Advertencia </strong> " + response.msj +" </div>";
        }
        else if(response.state==2)
        {
            alerts =  "<div class='alert alert-dismissible alert-danger'>" + 
            "<button type='button' class='close' data-dismiss='alert'>x</button>" + 
            "<span class='label label-info'>"+ response.date +"</span> <strong>Error </strong> " + response.msj +"</div>";
        }
        
        
        $("#message").prepend(alerts);
        $("#dni").val("");

        if( $("#message").children().length > 5)
        {
            var obj_first = $("#message").children().last();
            $( obj_first ).hide( "drop", { direction: "down" }, "slow", function complete() {
                $( obj_first ).remove();
            } );
        }

    });
}

function mostrar_hora(){

    var fecha = new Date();
    var hours = fecha.getHours();
    var minutes = fecha.getMinutes();
    var seconds = fecha.getSeconds();

    var dn="PM";
    if (hours<12)
        dn="AM";
    if (hours>12)
        hours=hours-12;
    if (hours==0)
        hours=12;

    if (minutes<=9)
        minutes="0" + minutes;
    if (seconds<=9)
        seconds="0" + seconds;

    $("#date").html(hours + ":" + minutes + ":" + seconds + " " + dn);
    setTimeout("mostrar_hora()",1000)
}

mostrar_hora();