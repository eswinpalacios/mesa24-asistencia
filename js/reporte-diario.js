$(function() {
    $( "#date_start" ).datepicker();
    $( "#date_start" ).datepicker( "option", "dateFormat", "yy-mm-dd" ).datepicker( "setDate", new Date());
    
    generar_reporte();

});

$( "#btn-generar-reporte" ).click(function() {	
	generar_reporte();
});

function generar_reporte()
{
	console.log("generar reporte");

	$.post( "reporte-diario-listado.php", { date_start: $("#date_start").val() })
	.done(function( data ) {
    	$( "#reporte" ).html( data );
  	});
}