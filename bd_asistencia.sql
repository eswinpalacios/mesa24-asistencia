-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Base de datos: `bd_asistencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(1) COLLATE ucs2_spanish_ci NOT NULL COMMENT 'i=ingreso s=salida',
  `fecha` datetime NOT NULL,
  `persona_dni` varchar(8) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Volcado de datos para la tabla `asistencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `dni` varchar(8) COLLATE ucs2_spanish_ci NOT NULL,
  `nombre` varchar(60) COLLATE ucs2_spanish_ci NOT NULL,
  `empresa` varchar(100) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`dni`, `nombre`, `empresa`) VALUES
('11111111', 'Juan Perez Baca', 'SES SAC'),
('22222222', 'Miguel Alonso ', 'Bit Solutions'),
('33333333', 'Luis Gonzales', 'Everis'),
('44444444', 'Jhon Barrantes', 'Indra SAC'),
('45840405', 'Eswin Palacios Sarmiento', 'USAT');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asistencia_persona_idx` (`persona_dni`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD CONSTRAINT `fk_asistencia_persona` FOREIGN KEY (`persona_dni`) REFERENCES `persona` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION;
