<?php

include "conexion.php";

if(!(isset($_POST['date_start'])))
    return; 

$date_start = $_POST['date_start'];

try {
    $sql = "select p.nombre, p.empresa, p.dni, a.tipo from persona p RIGHT join asistencia a on p.dni = a.persona_dni 
         where a.tipo='i' and a.fecha>='".$date_start." 00:00:00' and a.fecha<='".$date_start." 23:59:59'
          GROUP by nombre";
  $query = $mbd->query($sql);

} catch (PDOException $e) {
    return;
}

?>

<?php   if($query->rowCount()>0) {  ?>

<table class="table table-striped table-hover ">
    <thead>
        <tr>
            <th>#</th>
            <th>Fecha</th>
            <th>Nombre</th>
            <th>Empresa</th>
            <th>Tipo</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($query as $row) { ?>
    
    <?php if($row['tipo']=="i") { ?>
        <tr class="success">
    <?php } else { ?>
        <tr>
    <?php } ?>

        <td>1</td>
        <td><?php echo $date_start ?></td>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['empresa'] ?></td>
        <td><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></td>

    </tr>
<?php } ?>
    </tbody>
</table>

<?php   }    ?>