<?php

include "conexion.php";

if(!(isset($_POST['date_start']) && isset($_POST['date_finish'])))
    return; 

$date_start = $_POST['date_start'];
$date_finish = $_POST['date_finish'];

try {
    $sql = "select a.id, p.nombre, p.empresa, p.dni, a.fecha, a.tipo from persona p inner join asistencia a on p.dni = a.persona_dni where a.fecha>='".$date_start." 00:00:00' and a.fecha<='".$date_finish." 23:59:59' order by a.id desc ";
  $query = $mbd->query($sql);

} catch (PDOException $e) {
    return;
}

?>

<?php   if($query->rowCount()>0) {  ?>

<table class="table table-striped table-hover ">
    <thead>
        <tr>
            <th>#</th>
            <th>Fecha</th>
            <th>Nombre</th>
            <th>Empresa</th>
            <th>Tipo</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($query as $row) { ?>
    
    <?php if($row['tipo']=="i") { ?>
        <tr class="success">
    <?php } else { ?>
        <tr>
    <?php } ?>

        <td>1</td>
        <td><?php echo $row['fecha'] ?></td>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['empresa'] ?></td>
        
        <?php if($row['tipo']=="i") { ?>
            <td><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
        <?php } else { ?>
            <td><span class=" glyphicon glyphicon-log-out" aria-hidden="true"></span>
        <?php } ?>
        <td>

    </tr>
<?php } ?>
    </tbody>
</table>

<?php   }    ?>