
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asistencia - Evaluacion</title>    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Asistencia</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio</a></li>
              <li><a href="reporte.php"><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span> Reporte</a></li>
              <li><a href="reporte-diario.php"><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span> Reporte Diario</a></li>
            </ul>
           
          </div>
        </div>
      </nav>

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Control de Asistencia</h3>
      </div>
      <div class="panel-body">

      <h1>HORA <span id="date"></span></h1>
      
       
       <div class="form-group">        
        
        <div class="input-group">
          <span class="input-group-addon">DNI</span>
          <input class="form-control" id="dni" type="text" maxlength="8">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="button" id="btn-marcar" ><span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"> Marcar</button>
          </span>
        </div>

      </div>

      <div id="message"></div>

    </div>

    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/index.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

  </body>
</html>
