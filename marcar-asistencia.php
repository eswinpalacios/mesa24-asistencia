<?php
	
include "conexion.php";

if(!isset($_POST['dni']))
	$rpta = array('state' => 2, 'msj' => 'error .' );

else{
	$dni = $_POST['dni'];

    date_default_timezone_set("America/Lima");
    $fecha = date("Y-m-d H:i:s a");
	
    try {
        
        //check dni
        $query = $mbd->query("select dni,nombre, empresa from persona where dni like ".$dni);
        if( $query->rowCount() == 0 )
        {
        	$rpta = array('state' => 1, 'date' => $fecha, 'msj' => 'DNI no encontrado: '.$dni);
        }
        else
        {
        	foreach ($query as $row)
        		$nombre = $row['nombre'];
        	
            //check i/o
        	$sql = "select persona_dni,fecha,tipo from asistencia where persona_dni like ".$dni." order by id desc limit 1";
        	$query = $mbd->query($sql);
        	$tipo = "i";
            $msj = "";

        	if( $query->rowCount() > 0 )
        	{
        		foreach ($query as $row)
        			$tipo = $row['tipo'];
              
    		    if($tipo=="i")
    		    {
    		    	$tipo = "s";
    		    	$msj = 'Salida de '.$nombre;
    		    }
    		    else
    		    {
    		    	$tipo = "i";
    		    	$msj = 'Ingreso de '.$nombre;
    		    }
    		    $msj .= " DNI: ".$dni;
        	}
            else
                $msj = 'Ingreso de '.$nombre;

        	//assist
        	$query = $mbd->query("insert into asistencia (tipo, fecha, persona_dni) values ('".$tipo."','".$fecha."','".$dni."')");

        	$rpta = array('state' => 0, 'date' => $fecha, 'msj' => $msj);
        }        

        $mbd = null;
    } catch (PDOException $e) {
    	    $rpta = array('state' => 2, 'date' => $fecha, 'msj' => 'No se pudo registrar la asistencia del DNI: '.$dni );
    }

}

echo json_encode($rpta);

?>